from machine import I2C

i2c = I2C(I2C.I2C0, freq=100000, scl=28, sda=29)
devices = i2c.scan()
print(devices)
rdaaddr = 16
bootrom = b'\xc1\x03\x00\x00\x0a\x00\x88\x0f\x00\x00\x42\x02'

def rdreset():
	i2c.writeto(16,b'\x00\x02')

def rdinit():
	i2c.writeto(16,bootrom)

def tune(freq):
	fq = int((freq-87.0)*10.0+0.5)
	tc = b'\xc0\x01'+bytes([(fq>>2)&0xFF])+bytes([((fq&0x03)<<6)&0xFF|0x10])
	i2c.writeto(16,tc)

rdreset()  #复位
rdinit()   #初始化
tune(90.9) #调频